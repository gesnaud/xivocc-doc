.. xivo-cc-doc Documentation master file.

*********************
XiVO-CC Documentation
*********************

.. figure:: logo_xivo-cc.png
   :scale: 30%


XiVO-CC is software suite developed by Avencall_ Group, and provides enhancements of the XiVO_ PBX contact center functionalities.

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://www.xivo.io/

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   
   introduction/introduction
   features/index
   installation/index
   configuration/index
   administration/administration
   xuc/index
   upgrade/index
   troubleshooting/index
   developers/developers

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
