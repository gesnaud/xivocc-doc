*****************
Phone integration
*****************

XuC based web applications like agent interface or xivo client web integrates buttons for phone
control. This section details necessary configuration, supported phones and limitations.

.. note:: The XuC server need to be able to reach your phones through the VOIP network.

.. _phone_integration_supported:

Supported phones
================

+-----------------+---------------------------------------------------------------------------+
| Manufacturer    | Function                                                                  |
|                 +--------+--------+------+------------+-------------------+-----------------+
|                 | Answer | Hangup | Hold | Conference | Attended Transfer | Direct Transfer |
+=================+========+========+======+============+===================+=================+
| Snom 7XX        |   OK   |   OK   |  OK  |     OK     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+
| Polycom VVX     |   OK   |   OK   |  OK  |     NO     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+
| Yealink T4X     |   OK   |   OK   |  OK  |     NO     |        OK         |       OK        |
+-----------------+--------+--------+------+------------+-------------------+-----------------+

NO = Not available

.. _phone_integration_installation:

Required configuration
======================

Customize templates for Polycom phones
--------------------------------------

To enable phone control buttons on the web interface you must update the basic template of Polycom
phones:

* go to the plugin directory: ``/var/lib/xivo-provd/plugins/xivo-polycom-VERSION``
* copy the default template with ``cp templates/base.tpl var/templates/``
* edit ``var/templates/base.tpl`` around line 121, modify the ``app.push`` parameters for non
  switchboard phone like bellow :

.. code-block:: ini

  {% if XX_options['switchboard'] -%}
  apps.push.messageType="5"
  apps.push.username="xivo_switchboard"
  apps.push.password="xivo_switchboard"
  call.callWaiting.enable="0"
  {% else -%}
  apps.push.messageType="5"
  apps.push.username="guest"
  apps.push.password="guest"
  call.callWaiting.enable="1"
  {% endif %}

Customize templates for Yealink phones
--------------------------------------

To enable phone control buttons on the web interface you must update the basic template of Yealink
phones:

* go to the plugin directory: ``/var/lib/xivo-provd/plugins/xivo-yealink-VERSION``
* copy the default template from ``cp templates/base.tpl var/templates/``
* edit ``var/templates/base.tpl`` around line 120, modify the sip notify option for non switchboard
  phones like bellow :

.. code-block:: ini

  {% if XX_options['switchboard'] -%}
  push_xml.sip_notify = 1
  call_waiting.enable = 0
  {% else -%}
  push_xml.sip_notify = 1
  call_waiting.enable = 1
  {% endif %}

Update Device Configuration
---------------------------
* to update device configuration you must run :
  ``xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").reconfigure()'``
* and finally you must resynchronize the device:
  ``xivo-provd-cli -c 'devices.using_plugin("xivo-polycom-VERSION").synchronize()'``
* refer to provisioning_ documentation for more details
* if the phone synchronization fails check if the phone uses the version of the plugin you have
  updated, you can use ``xivo-provd-cli -c 'devices.find()'``

.. _provisioning : http://documentation.xivo.fr/en/stable/administration/provisioning/adv_configuration.html

